import { middyfy } from '../../libs/lambda';

const handler = async (event) => {

  const queries = event.queryStringParameters;

  const retorno = {
    nome: queries.name
  };

  return {
    statusCode: 200,
    body: JSON.stringify(retorno)
  };
};

export const main = middyfy(handler);