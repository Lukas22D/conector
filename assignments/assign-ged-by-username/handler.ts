import { middyfy } from '../../libs/lambda';

import axios from 'axios';

const PATH = '/platform/ecm_ged/queries/folderListPageable';
const PATH_USER = '/platform/user/queries/listUsers';
let response;

const assignmentGedByUser = async (event) => {

  try {

    let processPdfs = [] as { id: string, title: string }[];
    const url = event.headers['x-platform-environment'] + PATH;
    for (const folder of event.body.folder.split(';')) {
      const files = await axios.get(`${url}?id=${folder}&page=0&size=10000&onlyFolders=false`, {
        headers: { Authorization: event.headers["x-platform-authorization"] }
      });
      if (files.data?.files?.length) {
        processPdfs = processPdfs.concat(files.data.files.filter(f => f.mimetype === 'application/pdf'));
      }
    }

    const responseData = await axios.post(event.headers['x-platform-environment'] + PATH_USER, {
      users: [event.body?.user]
    }, {
      headers: { Authorization: event.headers['x-platform-authorization'] }
    });

    if (responseData.data?.users?.length !== 1) {
      throw new Error('Usuário inválido!');
    }

    const users = processPdfs
      .map(p => {
        return {
          documentId: p.id,
          email: responseData.data?.users[0].email,
          name: responseData.data?.users[0].fullName,
          phone: responseData.data?.users[0].phone
        }
      });


    const scheduling = new Date();

    response = {
      statusCode: 200,
      body: JSON.stringify(
        users?.map(user => ({
          context: user,
          scheduling
        }))
      )
    };

  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(assignmentGedByUser);