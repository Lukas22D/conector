import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const PATH = '/hcm/employeejourney/entities/department';

const hcmListDepartment = async (event) => {

  const url = event.headers['x-platform-environment'] + PATH;

  const filter = [] as string[];
  const tableCode = event?.queryStringParameters?.tableCode;
  const code = event?.queryStringParameters?.code;
  const name = event?.queryStringParameters?.name;

  if (tableCode) {
    filter.push(`tableCode eq '${tableCode}'`);
  }

  if (code) {
    filter.push(`code eq '${code}'`);
  }

  if (name) {
    filter.push(`name eq '${name}'`);
  }

  const page = event?.queryStringParameters?.page || 0;
  const size = event?.queryStringParameters?.size || 10;

  try {

    const responseData = await Axios.get(`${url}${filter.length ? '?filter=' : ''}${filter.length ? filter.join(' AND ') : ''}${filter.length ? '&' : '?'}size=${size}&offset=${page}`, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    const data = responseData.data;

    const departments = data.contents.map(department => {
      return {
        id: department.id,
        tableCode: department.tableCode,
        code: department.code,
        position: department.position,
        name: department.name,
        creationDate: department.creationDate,
        historicExpirationDate: department.historicExpirationDate
      };
    });

    const response = {
      contents: departments,
      totalPages: data.totalPages,
      totalElements: data.totalElements
    };

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(hcmListDepartment);