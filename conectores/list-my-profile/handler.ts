import { middyfy } from '../../libs/lambda';
import axios from 'axios';
import https from 'https';

const PATH = '/platform/social/queries/listMyProfiles'

const handler = async (event) => {
  
  const url = event.headers['x-platform-environment'] + PATH;

  const accessToken = event.headers["x-platform-authorization"];

  try {
    const response = await axios.get(url, {
      headers: { Authorization: accessToken },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      })
    });

    let profiles = response.data?.profiles;
    profiles = profiles.map(profile => {
      delete profile.spaces;
      delete profile.tags;
      return profile;
    })

    return {
      statusCode: response.status || 200,
      body: JSON.stringify(profiles)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

};

export const main = middyfy(handler);