import { middyfy } from '../../libs/lambda';
import Axios from 'axios';
import moment from 'moment';

const PATH = "/crm/account/entities/account";
let response;

const crmListAccount = async (event) => {

  const id = event?.queryStringParameters?.id;
  const name = event?.queryStringParameters?.name;
  const registerDate = event?.queryStringParameters?.registerDate;
  const active = event?.queryStringParameters?.active as string;
  const filter = [] as string[];

  if (id) {
    filter.push(`id=${id}`);
  }
  if (name) {
    filter.push(`name='${name}'`);
  }
  if (registerDate) {
    filter.push(`registerDate=${registerDate}`)
  }
  
  if (active) {
    filter.push(`active=${active.toLowerCase() === 'ativo'}`)
  }
  
  const page = event?.queryStringParameters?.page || 0;
  const size = event?.queryStringParameters?.size || 10;

  const url = event.headers['x-platform-environment'] + PATH;

  try {
    const responseData = await Axios.get(`${url}${filter.length ? '?filter=' : ''}${filter.length ? filter.join(' AND ') : ''}${filter.length ? '&' : '?'}size=${size}&offset=${page}`, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    response = {
      statusCode: responseData?.status || 200,
      body: JSON.stringify({
        contents: responseData.data.contents.map(content => {
          return {
            id: content.id,
            name: content.name,
            registerDate: moment(content.registerDate).format('DD/MM/YYYY'),
            active: (content?.active ? "Ativo" : "Inativo")
          }
        })
      })
    };
  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(crmListAccount);